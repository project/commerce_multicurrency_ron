The National Bank of Romania (bnr.ro) as currency exchange rate sync
provider for Commerce multicurrency module.

DEPENDENCIES
Commerce Multicurrency provider for RON depends on the Commerce multicurrency 
module.

INSTALLATION
Install the module as usual.

CONFIGURATION
1. Select "National Bank of Romania (BNR)" as sync provider on currency
   conversion settings page: admin/commerce/config/currency/conversion
2. Run cron or sync manually to synchronize the rates.
